//
//  ContentView.swift
//  tita
//
//  Created by Bryan Khufa on 03/06/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Ganti dua!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
