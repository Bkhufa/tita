//
//  titaApp.swift
//  tita
//
//  Created by Bryan Khufa on 03/06/21.
//

import SwiftUI

@main
struct titaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
